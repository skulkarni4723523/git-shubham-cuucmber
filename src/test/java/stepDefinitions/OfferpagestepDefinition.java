package stepDefinitions;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.LandingpageObject;
import pageObjects.OfferpageObject;
import pageObjects.PageObjectManager;
import utility.Testutility;

public class OfferpagestepDefinition {
	
	public WebDriver driver;
	
	public String landingproductname;
	
	public String offerPageproductname;
	
	public String offerprod;
	
	Testutility testutility;
	
	public  OfferpagestepDefinition(Testutility testutility) {
		
		this.testutility = testutility;
	}
	
	@Then("^User searched for (.+) in offers page to check$")
	public void user_searched_for_in_offers_page_to_check(String name) throws InterruptedException, IOException {
		
		 testutility.getpageObjectManager().getLandingPage().clickTopdeal();
		
         testutility.getSwichWindow().switchToWindow();
        
        testutility.getpageObjectManager().getOfferpage().enterSearchOffer(name);
		
		 Thread.sleep(2000);
		 
		 offerprod =  testutility.getpageObjectManager().getOfferpage().getOfferproduct();
		 
  	    System.out.println(offerprod);
		
	}
	
	@Then("Validate if product exists with same name")
	public void validate_if_product_exists_with_same_name() {

	  Assert.assertEquals(testutility.landprod,offerprod);
		
	}

}
