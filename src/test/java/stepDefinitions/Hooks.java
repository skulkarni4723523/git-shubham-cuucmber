package stepDefinitions;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Scenario;
import utility.Testutility;

public class Hooks {
	
	Testutility testutility;

	public Hooks(Testutility testutility) {
		
		this.testutility = testutility;
	}
	
	@After
    public void closeBrowser() throws IOException {
		
		testutility.testbase.getTestbase().quit();
	}
	
	@AfterStep
	public void getScreenshot(Scenario scenario) throws IOException {
		
		WebDriver driver = testutility.testbase.getTestbase();
		
		if(scenario.isFailed()) {
			
			File sourcePath = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
			
			byte[] fileContent = FileUtils.readFileToByteArray(sourcePath);
			
			scenario.attach(fileContent, "image/png", "greencart");
		}
	}
}
