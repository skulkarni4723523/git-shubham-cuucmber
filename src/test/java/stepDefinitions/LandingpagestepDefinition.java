package stepDefinitions;

import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.testng.Assert;
import org.testng.asserts.Assertion;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.LandingpageObject;
import pageObjects.PageObjectManager;
import utility.Testbase;
import utility.Testutility;

public class LandingpagestepDefinition {
	
	public WebDriver driver;
	
	public String offerPageproductname;
	
	Testutility testutility;
	
	Testbase testbase;
	
	public LandingpagestepDefinition(Testutility testutility) {
		
		this.testutility = testutility;
	}
	

	@Given("User is on Greencart landing page")
	public void user_is_on_greencart_landing_page() throws IOException {
    
	   testutility.getpageObjectManager();	
	}
	
	@When("^User searched with shortname (.+) and extracted actual name of product$")
	public void user_searched_with_shortname_and_extracted_actual_name_of_product(String name) throws IOException, InterruptedException {

	  testutility.getpageObjectManager().getLandingPage().enterSearch(name);
	  
	   Thread.sleep(5000);
	    
	    testutility.landprod = testutility.getpageObjectManager().getLandingPage().getLadibngproduct().split("-")[0].trim();
		
		System.out.println(testutility.landprod);
	}
	   
	
}
