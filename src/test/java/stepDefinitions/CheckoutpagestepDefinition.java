package stepDefinitions;

import java.io.IOException;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pageObjects.CheckoutpageObject;
import pageObjects.PageObjectManager;
import utility.Testbase;
import utility.Testutility;



public class CheckoutpagestepDefinition {
	
	public WebDriver driver;
	Testutility testutility;
	Testbase testbase;
	
	public CheckoutpagestepDefinition(Testutility testutility) {
		
		this.testutility = testutility;
	}
	
	@Given("user on landing page")
	public void user_on_landing_page() throws IOException {
	
	      Assert.assertEquals(testutility.testbase.getTestbase().getCurrentUrl().contains("rahulshettyacademy"),true);
			
	}
	
	@When("^user search with name (.+) in searchbox")
	public void user_search_name_in_searchbox(String name) throws IOException{
		
		testutility.getpageObjectManager().getCheckoutPage().enterSearch(name);
	}
	
	@When("user click on count icon")
	public void user_click_on_count_icon() throws IOException, InterruptedException {
		
		 testutility.getpageObjectManager().getCheckoutPage().clickQunatity();
	}
	
	@When("user click on add to cart")
	public void user_click_on_add_to_cart() throws IOException, InterruptedException {
		
		  testutility.getpageObjectManager().getCheckoutPage().clickAddToCart();
	}

	
	@When("user click on shop icon")
	public void user_click_on_shop_icon() throws IOException, InterruptedException {
		
		testutility.getpageObjectManager().getCheckoutPage().clickShopcart();
	}
	
	
	@When("user click on proceed to checkout")
	public void user_click_on_proceed_to_checkout() throws IOException, InterruptedException {
		
	   testutility.getpageObjectManager().getCheckoutPage().clickProceedToCheckout();
	}
	
	
	@Then("user get product name")
	public void user_get_product_name() throws IOException {
		
		String Pageprodcut = testutility.getpageObjectManager().getCheckoutPage().getPageproduct().getText();
		
		String procut = "Tomato - 1 Kg";
		
		if(Pageprodcut.equals(procut)) {
			
			Assert.assertEquals(Pageprodcut, procut);
			
			System.out.println("sucess");
		}
				
	}
	
	@When("user click on place order")
	public void user_click_on_place_order() throws InterruptedException, IOException{
		
		testutility.getpageObjectManager().getCheckoutPage().clickPlaceOrder();
	}
	
	@Then("user land on country page")
	public void user_land_on_country_page() throws InterruptedException, IOException {
		
		Assert.assertEquals(testutility.testbase.getTestbase().getCurrentUrl().contains("country"), true);
		
		System.out.println("country page");
	}
	
	
	@When("user select country")
	public void user_select_country() throws InterruptedException, IOException {
		testutility.getpageObjectManager().getCheckoutPage().selectCountry();
	}
	
	
	@When("user click on terms")
	public void user_click_on_terms() throws InterruptedException, IOException {
		
		testutility.getpageObjectManager().getCheckoutPage().clickTerms();
	}
	
	@When("user click on proceed button")
	public void user_click_on_proceed_button() throws InterruptedException, IOException {
		
		testutility.getpageObjectManager().getCheckoutPage().clickProceed();
	}
	
	@Then("user should see thank you message")
	public void user_see_thank_you_message() throws IOException {
		
		testutility.getpageObjectManager().getCheckoutPage().thankYou();
	}
}
