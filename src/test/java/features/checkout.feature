Feature: Checkout page

@PlaceOrder
Scenario Outline: Test functionaliy of checkout page

Given user on landing page

When user search with name <name> in searchbox
When user click on count icon
And user click on add to cart 
And user click on shop icon 
And user click on proceed to checkout
Then user get product name 
When user click on place order
Then user land on country page 
When user select country 
And user click on terms 
And user click on proceed button
Then user should see thank you message

Examples:
|name |
|Tom |
