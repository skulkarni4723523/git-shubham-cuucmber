#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@tag
Feature: Application login 
  I want to use this template for my feature file

  @SearchOffer
  Scenario Outline: To check search functionality 
    Given User is on Greencart landing page 
    When User searched with shortname <Name> and extracted actual name of product
    Then User searched for <Name> in offers page to check  
    And Validate if product exists with same name
    
   Examples:
   |Name |
   |Tom  |
   |Beet |
 

    
    


