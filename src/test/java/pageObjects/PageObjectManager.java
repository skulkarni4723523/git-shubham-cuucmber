package pageObjects;

import org.openqa.selenium.WebDriver;

import utility.Testutility;

public class PageObjectManager {
	
	public WebDriver driver;
	
	public LandingpageObject landingpageObject;
	
	public OfferpageObject offerpageObject;
	
	public CheckoutpageObject checkoutpageObject;
	
	public PageObjectManager(WebDriver driver) {
		
		this.driver = driver;
	}
	
	public LandingpageObject getLandingPage() {
		
		return landingpageObject = new LandingpageObject(driver);
	}

	public OfferpageObject getOfferpage() {
		
		return offerpageObject = new OfferpageObject(driver);
	}
	
	public CheckoutpageObject getCheckoutPage() {
		
		return checkoutpageObject = new CheckoutpageObject(driver);
	}
}
