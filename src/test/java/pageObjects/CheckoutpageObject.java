package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import utility.Testbase;

public class CheckoutpageObject {
	
	public WebDriver driver;
	
	By quanity = By.xpath("//a[text()='+']");
	
	By Add_cart = By.xpath("//button[text()='ADD TO CART']");
	
	By shopcart = By.xpath("//img[@alt=\"Cart\"]");
	

	By proceed_to_checkout = By.xpath("//button[text()='PROCEED TO CHECKOUT']");
	
	By pageproduct = By.xpath("//p[text()='Tomato - 1 Kg']");
	
	By searchcHECKOUT = By.xpath("//input[@type='search']");
	
	By placeOrder = By.xpath("//button[text()='Place Order']");
	
	By country = By.xpath("//select[@style=\"width: 200px;\"]");
	
	By terms = By.xpath("//input[@type='checkbox']");
	
	By proceed = By.xpath("//button[text()='Proceed']");
	
	
	public CheckoutpageObject(WebDriver driver) {
		
		this.driver = driver;
	}

	public void enterSearch(String name) {
		
		driver.findElement(searchcHECKOUT).sendKeys(name);
	}
	
	public void clickQunatity() throws InterruptedException {
		
         for(int i=1; i<4; i++) {
			
			driver.findElement(By.xpath("//a[text()='+']")).click();
			Thread.sleep(2000);
		
	    }
			
	}
	
	public void clickAddToCart() throws InterruptedException {
		
		driver.findElement(Add_cart).click();
		Thread.sleep(2000);
	}
	
	
    public void clickShopcart() throws InterruptedException {
    	
    	driver.findElement(shopcart).click();
    	Thread.sleep(2000);
    }
    
    public void clickProceedToCheckout() throws InterruptedException {
    	
    	driver.findElement(proceed_to_checkout).click();
    	Thread.sleep(2000);
    }
    
    public WebElement getPageproduct() {
    
        return driver.findElement(pageproduct);
    }
    
    public void clickPlaceOrder() throws InterruptedException {
    	
    	driver.findElement(placeOrder).click();
    	Thread.sleep(2000);
    }
    
    public void selectCountry() throws InterruptedException {
    	
    	WebElement dropdown = driver.findElement(country);
    	
    	Select selectDropdown = new Select(dropdown);
    	
    	Thread.sleep(2000);
    	
    	selectDropdown.selectByVisibleText("Iran");	
    	
    	Thread.sleep(1000);
    }
    
    public void clickTerms() throws InterruptedException {
    	
    	driver.findElement(terms).click();
    	Thread.sleep(1000);
    }
    
    public void clickProceed() throws InterruptedException {
    	
    	driver.findElement(proceed).click();
		Thread.sleep(4000);
	}
    
    public void thankYou() {
    	
    	Assert.assertEquals(driver.getPageSource().contains("Thank you, your order has been placed successfully "),true);
    	
    	System.out.println("thank you");
    }
}
