package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LandingpageObject {
	
	public WebDriver driver;
	
    public LandingpageObject(WebDriver driver) {
		
		this.driver = driver;
	}
	
	private By search = By.xpath("//input[@type='search']");
	
	private By  landingproductname = By.xpath("//h4[text() ='Tomato - 1 Kg']");
	
	private By topdeal = By.xpath("//a[text()='Top Deals']");
	

	public void enterSearch(String name) {
		
		driver.findElement(search).sendKeys(name);
		
	}
	
	public String getLadibngproduct() {
		
		return driver.findElement(By.xpath("//h4[text() ='Tomato - 1 Kg']")).getText();
	}
	
	public void clickTopdeal() {
		
		driver.findElement(topdeal).click();
	}

}
