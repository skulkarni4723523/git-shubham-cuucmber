package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class OfferpageObject {
	
	public WebDriver driver;
	
    public OfferpageObject(WebDriver driver) {
		
		this.driver = driver;
	}
	
	private By searchOffer = By.xpath("//input[@id = 'search-field']");
	
	private By offerPageproductname = By.xpath("//tr[1]/td[1]");
	
	

	public void enterSearchOffer(String name) {
		
		driver.findElement(searchOffer).sendKeys(name);
		
	}
	
	public String getOfferproduct() {
		
		return driver.findElement(offerPageproductname).getText();
	}

}
