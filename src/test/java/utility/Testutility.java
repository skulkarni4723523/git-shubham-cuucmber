package utility;

import java.io.IOException;

import org.openqa.selenium.WebDriver;

import pageObjects.PageObjectManager;
import stepDefinitions.LandingpagestepDefinition;

public class Testutility {
	
	public WebDriver driver;
	
	public String landprod;
	
	public Testbase testbase;
	
    public PageObjectManager pageObjectManager;
    
    public GenericReusableUtils genericReusableUtils;
	
    public Testutility(Testbase testbase) {
    	
    	this.testbase = testbase;
    }
    
	public PageObjectManager getpageObjectManager () throws IOException {
		
		return pageObjectManager = new PageObjectManager(testbase.getTestbase());
	}

	
	public GenericReusableUtils getSwichWindow() throws IOException {
		
		return genericReusableUtils = new GenericReusableUtils(testbase.getTestbase());
	}
}
