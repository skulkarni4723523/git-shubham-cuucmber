package utility;

import java.io.FileInputStream;

import java.io.IOException;
import java.time.Duration;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.edge.EdgeDriver;

public class Testbase {
	
	public WebDriver driver;
	
	Testutility testutility;
	
	public WebDriver getTestbase() throws IOException {
		
		FileInputStream fileInputStream = new FileInputStream(System.getProperty("user.dir")+"\\src\\test\\resource\\global.properties");
		
		Properties properties = new Properties();
		
		properties.load(fileInputStream);
		
		String URL = properties.getProperty("QAUrl");
	
		if(driver==null) {
			if(properties.getProperty("browser").equalsIgnoreCase("edge")) {
	               System.setProperty("webdriver.edge.driver", "C:\\Users\\wbox62\\eclipse-workspace\\cucumber\\target\\msedgedriver.exe");
	               driver = new EdgeDriver();
			}
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	      driver.get(URL);
        }
		
	return driver;
	
	}

}
