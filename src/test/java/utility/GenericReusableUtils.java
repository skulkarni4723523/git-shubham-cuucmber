package utility;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.WebDriver;

public class GenericReusableUtils {
	
	public WebDriver driver;
	
	public GenericReusableUtils(WebDriver driver) {
		
		this.driver = driver;
	}
	
	public void switchToWindow() {
		
		  Set<String> windows = driver.getWindowHandles();
			
			Iterator i = windows.iterator();
			
			String parentwindow = (String) i.next();
			
			String childwindow = (String) i.next();
			
			driver.switchTo().window(childwindow);
	}
}
